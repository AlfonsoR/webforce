<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.app');
    }

    /**
     * Validate the credential of then user to give acces to the data table 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function auth(Request $request)
    {
        $input = $request->all();
        if(auth()->attempt(['email' => $input['user'], 'password' => $input['password']])){
            return response()->json(['response' => true, 'msg' => 'Welcomem back']);
        }else{
            return response()->json(['response' => false, 'msg' => 'User or Password incorrect']);
        }
    }

}
