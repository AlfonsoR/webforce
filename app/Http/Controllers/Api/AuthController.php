<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User; 
use Hash;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request["password"] = Hash::make($request->password);
        $user = User::create($request->input());
        $success['token'] = $user
            ->createToken('tasks api')
            ->accessToken;

        return response()->json($success);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }

    public function login(Request $request)
    {
        if (auth()->attempt(request()->input())) {
            $user = auth()->user();
            $success['token'] = $user
                ->createToken('Passport Api')
                ->accessToken;
            return response()->json($success, 200);

        } else {
            return response()->json(['response' => false, 'msg' => 'User or Password incorrect'], 401);
        }
    }

    public function getUser(Request $request){
        return $request->user();
    }

}
