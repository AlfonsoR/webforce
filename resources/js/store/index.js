export default {
	state: {
        msg: 'test vuex',
        products: [],
        product: {},
        counter: 0,
	},
    mutations: {
        addProduct (state) {
            state.products.push(state.product)
            state.counter++;
        },
	},
	actions: {
      addProductAction (context){
        context.commit('addProduct');
      },
	},
    getters: {
        message ( state ){
            return state.msg
        }
	}
}