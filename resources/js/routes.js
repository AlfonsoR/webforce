import Vue from 'vue';
import Router from 'vue-router';


import Product from '../assets/js/views/ProductComponent';
import Detail from '../assets/js/views/DetailProductComponent';
import Checkout from '../assets/js/views/CheckOutComponent';
import Login from '../assets/js/views/LoginComponent';
import Register from '../assets/js/views/RegisterComponent';
import Admin from '../assets/js/views/AdminComponent';
import AdminProduct from '../assets/js/views/AdminProductComponent';
import AdminUser from '../assets/js/views/AdminUserComponent';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Product
        },
        {
            path: '/products/:slug',
            name: 'product_details',
            component: Detail
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin
        },
        {
            path: '/admin/products',
            name: 'admin_product',
            component: AdminProduct
        },
        {
            path: '/admin/users',
            name: 'admin_user',
            component: AdminUser
        }

    ],
    mode: 'history'
})