<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create(); // User to test login Email: admin@test.com Password: - password. This
        // user is created in the UserFactory
        \App\Models\Product::factory(30)->create(); // Create 30 Produts in the database
    }
}
